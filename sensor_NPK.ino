// Menggunakan ESP32


// const byte code[]= {0x01, 0x03, 0x00, 0x1e, 0x00, 0x03, 0x65, 0xCD};
const byte nitro[] = {0x01, 0x03, 0x00, 0x1e, 0x00, 0x01, 0xe4, 0x0c};
const byte phos[] = {0x01, 0x03, 0x00, 0x1f, 0x00, 0x01, 0xb5, 0xcc};
const byte pota[] = {0x01, 0x03, 0x00, 0x20, 0x00, 0x01, 0x85, 0xc0};

byte values[11];
#define RXD1 17
#define TXD1 16

void setup()
{
  Serial.begin(115200);
  Serial1.begin(4800, SERIAL_8N1, RXD1, TXD1); // Baudrate modbus 4800
}

void loop()
{
}

void baca_npk()
{
  byte val1, val2, val3;
  val1 = nitrogen();
  delay(250);
  val2 = phosphorous();
  delay(250);
  val3 = potassium();
  delay(250);

  Serial.print("Nitrogen: ");
  Serial.print(val1);
  Serial.println(" mg/kg");
  Serial.print("Phosphorous: ");
  Serial.print(val2);
  Serial.println(" mg/kg");
  Serial.print("Potassium: ");
  Serial.print(val3);
  Serial.println(" mg/kg");
}

byte nitrogen()
{

  delay(100);
  if (Serial1.write(nitro, sizeof(nitro)) == 8)
  {
    for (byte i = 0; i < 7; i++)
    {
      values[i] = Serial1.read();
      Serial.print(values[i], HEX);
    }
    Serial.println();
  }
  return values[4];
}

byte phosphorous()
{

  delay(100);
  if (Serial1.write(phos, sizeof(phos)) == 8)
  {
    for (byte i = 0; i < 7; i++)
    {
      values[i] = Serial1.read();
      Serial.print(values[i], HEX);
    }
    Serial.println();
  }
  return values[4];
}

byte potassium()
{

  delay(100);
  if (Serial1.write(pota, sizeof(pota)) == 8)
  {
    for (byte i = 0; i < 7; i++)
    {
      values[i] = Serial1.read();
      Serial.print(values[i], HEX);
    }
    Serial.println();
  }
  return values[4];
}
